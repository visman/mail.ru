import os
import sys
import argparse


def fail(msg):
    sys.stderr.write("%s\n" % msg)
    sys.exit(1)


def done(msg):
    sys.stderr.write("%s\n" % msg)
    sys.exit(0)


class PgrepLauncher(object):

    def __init__(self):
        self._args = self._parse_cli_args()
        self._curdir = os.path.abspath(os.path.dirname(__file__))
        self._lnum = 0
        self._fname = ''
        self._fname_list = []

    def fail(self, msg):
        fail(msg)

    def done(self, msg):
        done(msg)

    def _parse_cli_args(self):
        parser = argparse.ArgumentParser(description="Python grep")
        parser.add_argument('fstring', metavar='text', type=str, help='searched text')
        parser.add_argument('flist', metavar='file_name', type=str, nargs='+', help='file name to grep')
        parser.add_argument('-n', action='store_true', help='Line numbers')
        parser.add_argument('-f', action='store_true', help='File names')
        parser.add_argument('-i', action='store_true', help='Case insensitive')
        parser.add_argument('-v', action='store_true', help='Invert')

        return parser.parse_args()

    def _read_file(self, fname):
        self._lnum = 0
        self._fname = fname
        with open(os.path.join(self._curdir, fname), 'r') as _f:
            for line in _f:
                self._lnum += 1
                yield line

    def _find_pattern(self, lines, pattern=None, invert=False, case_sens=False):
        for line in lines:
            if case_sens:
                if pattern.lower() in line.lower() and not invert:
                    self._fname_list.append(self._fname)
                    yield line
                elif pattern.lower() not in line.lower() and invert:
                    yield line
            else:
                if pattern in line and not invert:
                    self._fname_list.append(self._fname)
                    yield line
                elif pattern not in line and invert:
                    yield line

    def run(self):
        cmd_args = self._args
        for file in cmd_args.flist:
            lines = self._read_file(file)
            result = self._find_pattern(lines, cmd_args.fstring, cmd_args.v, cmd_args.i)
            for res in result:
                if cmd_args.f:
                    continue
                if cmd_args.n:
                    print(self._lnum)
                    continue
                elif not cmd_args.n:
                    print(self._lnum, res)
                    continue
        if cmd_args.f:
            for item in set(self._fname_list):
                print(item)
        self.done('Command complete success!')


def main():
    PgrepLauncher().run()


if __name__ == '__main__':
    main()
